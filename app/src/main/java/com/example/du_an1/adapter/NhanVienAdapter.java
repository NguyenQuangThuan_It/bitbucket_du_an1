package com.example.du_an1.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.du_an1.R;
import com.example.du_an1.dao.NhanVien_DAO;
import com.example.du_an1.model.NhanVien;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
public class NhanVienAdapter extends RecyclerView.Adapter<NhanVienAdapter.ViewHolder> {

        Context context;
        ArrayList<NhanVien> danhsach;
        NhanVien_DAO nhanVien_dao;
        TextView tveditsdtkh;
        EditText edt_ten_nhanvien_sua, edt_sdt_nhanvien_sua,edt_suapass,edt_pass;
    private  static View.OnClickListener listener;
    public  interface OnItemClickListener{
        void OnItemClick(View itemview, int position);
    }
    public NhanVienAdapter( ArrayList<NhanVien> danhsach,Context context){
        super();
        this.danhsach = danhsach;
        this.context = context;

    }

    @NonNull
    @Override
    public NhanVienAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemview = inflater.inflate(R.layout.nhanvien_1row, viewGroup, false);
        return new ViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        int i = position + 1;
        viewHolder.stt.setText(i + "");
        viewHolder.txtten.setText(danhsach.get(position).getTenNV());
        viewHolder.txtsdt.setText(danhsach.get(position).getSDT());
        viewHolder.txtmanv.setText(danhsach.get(position).getMaNV());


        viewHolder.imv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(((Activity)context).findViewById(R.id.rv_nhanvien), "Xóa Nhân viên " +danhsach.get(position).getTenNV(), 3500)
                        .setActionTextColor(Color.RED)
                        .setAction("Đồng ý!", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                NhanVien nhanVien = danhsach.get(position);
                                nhanVien_dao = new NhanVien_DAO(context);
                                if (nhanVien_dao.deleteNhanVien(nhanVien)==-1){
                                    Toast.makeText(context, "Xóa không thành công", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                Toast.makeText(context, "Đã xóa khách hàng  " +danhsach.get(position).getTenNV(), Toast.LENGTH_SHORT).show();
                                danhsach.clear();
                                danhsach.addAll(nhanVien_dao.getAllnhanvien());
                                NhanVienAdapter.this.notifyDataSetChanged();
                            }
                        }).show();
            }});
        viewHolder.imv_edit.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder alertdialog = new AlertDialog.Builder(v.getContext());
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                v = inflater.inflate(R.layout.alert_suanhanvien, null);
                alertdialog.setView(v);
                final TextView sdt_nhanvien_sua = v.findViewById(R.id.txt_sdt_nhanvien);
                edt_ten_nhanvien_sua = v.findViewById(R.id.edt_tennv_nhanvien);
                edt_pass = v.findViewById(R.id.edt_pass_nhanvien_sua);
                edt_suapass = v.findViewById(R.id.edt_repass_nhanvien_sua);
                NhanVien kh = danhsach.get(position);


                // Set lên edittext sẵn khi click chỉnh sửa
                edt_ten_nhanvien_sua.setText(kh.getTenNV());
                tveditsdtkh.setText("Chỉnh sửa số điện thoại " + kh.getSDT());
                alertdialog.setPositiveButton("Cập nhật", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NhanVien nhanVien = danhsach.get(position);
                        nhanVien_dao = new NhanVien_DAO(context);
                        nhanVien.setTenNV(edt_ten_nhanvien_sua.getText().toString());
                        nhanVien.setSDT(edt_sdt_nhanvien_sua.getText().toString());
                        nhanVien.setMatKhau(edt_pass.getText().toString());
                        String ten = edt_ten_nhanvien_sua.getText().toString();
                            String mk = edt_pass.getText().toString();
                            String remk = edt_suapass.getText().toString();
                        if (ten.length() == 0 ) {
                            Toast.makeText(context, "Chưa nhập thông tin", Toast.LENGTH_SHORT).show();
                        } else if (!mk.equals(remk)) {
                            Toast.makeText(context, "Xác nhận mật khẩu chưa chính xác", Toast.LENGTH_SHORT).show();
                        } else {
                            if (nhanVien_dao.updateNhanVien(nhanVien) == -1) {
                                Toast.makeText(context, "Cập nhật không thành công", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Toast.makeText(context, "Đã cập nhật lại " + danhsach.get(position).getTenNV(), Toast.LENGTH_SHORT).show();
                            danhsach.clear();
                            danhsach.addAll(nhanVien_dao.getAllnhanvien());
                            NhanVienAdapter.this.notifyDataSetChanged();}

                    }
                });
                alertdialog.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertdialog.show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtten, txtsdt, txtmanv, stt;
        ImageView imv_delete,imv_edit;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imv_delete =(ImageView)itemView.findViewById(R.id.imv_delete);
            imv_edit =(ImageView)itemView.findViewById(R.id.imv_edit);
            txtten =(TextView)itemView.findViewById(R.id.tennv_1row);
            txtmanv=(TextView)itemView.findViewById(R.id.manv_nhanvien_1row);
            txtsdt=(TextView)itemView.findViewById(R.id.sdt_nhanvien_1row);
            stt =(TextView)itemView.findViewById(R.id.stt);
        }
    }
}
