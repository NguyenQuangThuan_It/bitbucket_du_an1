package com.example.du_an1.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.du_an1.R;
import com.example.du_an1.adapter.NhanVienAdapter;
import com.example.du_an1.dao.NhanVien_DAO;
import com.example.du_an1.model.NhanVien;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.ScaleInBottomAnimator;

public class Fragment_NhanVien extends Fragment {
    NhanVien_DAO nhanVien_dao;
    ArrayList<NhanVien>list;
    NhanVienAdapter nhanVienAdapter;
    public Fragment_NhanVien(){}
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_nhanvien,container,false);


        FloatingActionButton fab = view.findViewById(R.id.fab_nhanvien);
        list = new ArrayList<>();
        nhanVien_dao = new NhanVien_DAO(getActivity());
        list = nhanVien_dao.getAllnhanvien();
        nhanVienAdapter = new NhanVienAdapter(list,getContext());
        // Thêm recyclerView vào
        final RecyclerView recyclerView = view.findViewById(R.id.rv_nhanvien);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new ScaleInBottomAnimator());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(nhanVienAdapter);



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                LayoutInflater inflater = Fragment_NhanVien.this.getLayoutInflater();
                v = inflater.inflate(R.layout.alert_nhanvien, null);
                builder.setView(v);
                final EditText edt_tennv = v.findViewById(R.id.edttennv_them_nhanvien);
                final EditText edt_sdtnv = v.findViewById(R.id.edt_sdt_them_nhanvien);
                final EditText edt_ngaysinhnv = v.findViewById(R.id.edt_ngaysinh_them_nhanvien);
                final EditText edt_manv = v.findViewById(R.id.edt_manv_them_nhanvien);

                builder.setPositiveButton("Thêm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String sodienthoai = edt_sdtnv.getText().toString();
                        Boolean checksdt = nhanVien_dao.checkSDT(sodienthoai);
                        if (edt_tennv.getText().length() == 0 ||
                                edt_sdtnv.getText().length() == 0 ||
                                edt_ngaysinhnv.getText().length() == 0||
                                edt_manv.getText().length()==0
                        ) {
                            Toast.makeText(getContext(), "Chưa nhập thông tin", Toast.LENGTH_LONG).show();
                        } else if (checksdt == false) {
                            Toast.makeText(getContext(), "Số điện thoại đã tồn tại", Toast.LENGTH_LONG).show();
                            return;
                        } else {
                            NhanVien nhanVien = new NhanVien();
                            nhanVien.setTenNV(edt_tennv.getText().toString());
                            nhanVien.setSDT(edt_sdtnv.getText().toString());
                            nhanVien.setMaNV(edt_manv.getText().toString());
                            nhanVien.setNgaySinh(edt_ngaysinhnv.getText().toString());

                            if (nhanVien_dao.addNhanVien(nhanVien) == -1) {
                                Toast.makeText(getContext(), "Thêm không thành công", Toast.LENGTH_LONG).show();
                                return;
                            }
                            Toast.makeText(getContext(), "Thêm " + edt_sdtnv.getText().toString() + " thành công", Toast.LENGTH_SHORT).show();
                            list = nhanVien_dao.getAllnhanvien();
                            nhanVienAdapter = new NhanVienAdapter(list, getContext());
                            recyclerView.setAdapter(nhanVienAdapter);
                        }
                    }
                });
                builder.setNegativeButton("Thoát", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
        return view;
    }
}
