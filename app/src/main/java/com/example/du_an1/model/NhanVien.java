package com.example.du_an1.model;

import java.util.Date;

public class NhanVien {

    private String MaNV;
    private String TenNV;
    private String NgaySinh;
    private String NgayVaoLam;
    private String Luong;
    private String SDT;
    private String MatKhau;

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getMatKhau() {
        return MatKhau;
    }

    public void setMatKhau(String matKhau) {
        MatKhau = matKhau;
    }

    public String getMaNV() {
        return MaNV;
    }

    public void setMaNV(String maNV) {
        MaNV = maNV;
    }

    public String getTenNV() {
        return TenNV;
    }

    public void setTenNV(String tenNV) {
        TenNV = tenNV;
    }

    public String getNgaySinh() {
        return NgaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        NgaySinh = ngaySinh;
    }

    public String getNgayVaoLam() {
        return NgayVaoLam;
    }

    public void setNgayVaoLam(String ngayVaoLam) {
        NgayVaoLam = ngayVaoLam;
    }

    public String getLuong() {
        return Luong;
    }

    public void setLuong(String luong) {
        Luong = luong;
    }

    public NhanVien() {
    }

    public NhanVien(String maNV, String tenNV, String ngaySinh, String ngayVaoLam, String luong , String SDT, String matKhau) {
        MaNV = maNV;
        TenNV = tenNV;
        NgaySinh = ngaySinh;
        NgayVaoLam = ngayVaoLam;
        Luong = luong;
        this.SDT = SDT;
        MatKhau = matKhau;
    }
}
